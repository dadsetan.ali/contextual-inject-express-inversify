import "reflect-metadata";
import { Container } from 'inversify';
import { GenericRepository, /*GenericInteractor */} from './abstractAndInterfaces';
import { Entity, Interactor, Repository } from './implementations';
import TYPES from './types';

let container = new Container();

// container.bind<GenericInteractor<Entity>>(Interactor).toSelf();
container.bind<Interactor>(Interactor).toSelf();
container.bind<GenericRepository<Entity>>(TYPES.SERVICE).to(Repository);

export default container;