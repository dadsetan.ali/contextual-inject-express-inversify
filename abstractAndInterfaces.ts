import { injectable } from "inversify";

export type GenericEntity = {
    x: number
}

export interface GenericRepository<T> {
    get(): T[];
}

@injectable()
export abstract class GenericInteractor<T extends GenericEntity>{
    protected abstract repository: GenericRepository<T>;
    get(): T[] {
        return this.repository.get().filter(({ x }) => x > 0);
    }
}