import container from "./bootstrap";
// import { GenericInteractor, /*GenericRepository */ } from "./abstractAndInterfaces";
import { /*Entity,*/ Interactor } from "./implementations";
// import TYPES from "./types";

// let interactor = container.get<GenericInteractor<Entity>>(Interactor);
let interactor = container.get<Interactor>(Interactor);
console.log(interactor.get());
// let repository = container.get<GenericRepository<Entity>>(TYPES.SERVICE);
// console.log(repository.get());