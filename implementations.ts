import { GenericInteractor, GenericRepository, GenericEntity } from "./abstractAndInterfaces";
import { inject, injectable } from "inversify";
import TYPES from "./types";

export class Entity implements GenericEntity{
    constructor(public x: number){}
}

@injectable()
export class Repository implements GenericRepository<Entity>{
    get() {
        return [{ x: 1 }, { x: 0 }, { x: 2 }];
    }
}

@injectable()
export class Interactor extends GenericInteractor<Entity>{
    constructor(@inject(TYPES.SERVICE) protected repository: GenericRepository<Entity>) {
        super();
    }
}
